'use strict'
"use strict"

$(function () {

    // createAccordion();
    // removePrintTypeHref();

    let includedSections = $(".property:contains('Composed of'), .property:contains('Object of')")

    // Check if item has included book sections
    if (includedSections.length) {
        let includedSectionsIDs = getIncludedItemIDs();
        getIncludedItemValues(includedSectionsIDs);
    } else {
        return;
    }

    // If yes, get item id of included objects
    function getIncludedItemIDs() {

        let links = []
        let IDs = []

        // Only get ids of direct descendants of includedSections
        links.push(includedSections.find('.items a'))

        for (let i = 0; i < links[0].length; i++) {
            IDs.push(links[0][i].href.substring(links[0][i].href.lastIndexOf('/') + 1))
        }
        console.log(IDs)
        return IDs
    }

    function getIncludedItemValues(includedSectionsIDs) {

        // Each included book section has a lot of properties, many of which we are not interested in. These are the relevant ones.
        let relevantProperties = [
            //'bdm2:isPartOf', Should be inverse of carriesWork.
            'bdm2:hasType',
            'rdfs:label',
            'bdm2:hand',
            'dcterms:creator',
            'bdm2:comment',
            'bdm2:carriesWork',
            'bdm2:carriedOutBy',
            'bdm2:movesTo',
            'bdm2:position',
            'bdm2:productionDate',
            'bdm2:language',
            'bdm2:dateCreated',
            'bdm2:writingSupport',
            'bdm2:numberColumns',
            'bdm2:numberLines',
            'bdm2:numberLeaves'
        ];

        // Fetch all properties related to each included work ID
        for (let i = 0; i < includedSectionsIDs.length; i++) {
            let propertiesToWrite = [];

            $.get(`/api/items/${includedSectionsIDs[i]}`, function (data) {
                // Filter out irrelevant properties
                for (const property in data) {
                    if (relevantProperties.includes(property)) {
                        propertiesToWrite.push({ key: property, value: data[property] })
                    }
                }
                // Pass the data to our function that actually writes to the HTML
                findCorrectHTMLElement(propertiesToWrite, includedSectionsIDs[i])
            });
        }
    }

    function findCorrectHTMLElement(propertiesToWrite, includedSectionsID) {
        // Ensure that we write to the correct HTML child. This is done by matching our ID with the id in the href of the html child.
        // THIS CHECK HAS TO BE MADE BULLETPROOF! AS OF NOW IT ISNT - I THINK?
        let correctHTML = $('.items a[href*="' + getPathName() + '' + includedSectionsID + '"]').parent();
        writeProperties(propertiesToWrite, correctHTML, includedSectionsID);
    }

    function writeProperties(propertiesToWrite, correctHTML, includedSectionsID) {
        // Append our custom accordionContainer class as well as the id of the included work to the HTML element
        correctHTML.append('<details class=\"detailsContainer_' + includedSectionsID + '\">');

        let detailsContainer = $(".detailsContainer_" + includedSectionsID + "");
        detailsContainer.append('<summary>Click for details...</summary>');

        // Write the properties to the detailsContainer class
        for (let i = 0; i < propertiesToWrite.length; i++) {
            if (propertiesToWrite[i].value[0]['@value'] && propertiesToWrite[i].key != 'rdfs:label') {
                detailsContainer.append(
                    "<div class=\"property\"><h4>" +
                    propertiesToWrite[i].value[0].property_label +
                    "</h4> <div class=\"actionValues\"> <div class=\"actionValue\" lang>" +
                    propertiesToWrite[i].value[0]['@value'] +
                    "</div></div>" +
                    "</div>");
            } else if (propertiesToWrite[i].key == 'bdm2:carriedOutBy' || propertiesToWrite[i].key == 'bdm2:movedTo' || propertiesToWrite[i].key == 'bdm2:hasType') {
                detailsContainer.append(
                    "<div class=\"property\"><h4>" +
                    propertiesToWrite[i].value[0].property_label +
                    "</h4> <div class=\"accordionvalues\"> <div class=\"accordionvalue\" lang> " + createLink(propertiesToWrite[i].value[0]) +
                    propertiesToWrite[i].value[0].display_title +
                    "</a>" +
                    "</div></div>" +
                    "</div>");
            }
        }
        // Append a link to the item page of the work item
        detailsContainer.append("<div class=\"property\"><h4>Link to resource</h4>" + "<div class=\"values\"> <div class=\"value\" lang>" + "<a href=" + getPathName() + includedSectionsID + " target=_blank>" + 'Click to open in new window' + "</a>" + "</div></div></div > ");
        correctHTML.append('</details>')
    }
});

function createLink(objectID) {
    let id = objectID['@id'].substring(objectID['@id'].lastIndexOf('/') + 1)
    console.log('objectID: ', id)
    return `<a href=${getPathName() + id} target="_blank" style="color: #0000FF">`;
}

function createAccordion() {
    $(".property:contains('Composed of') .values .resource").accordion({
        collapsible: true,
        active: false,
        animate: false
    });
}

function getPathName() {
    let pathName = window.location.pathname;
    return pathName.replace(/\d/g, '')
}

function refreshAccordion() {
    includedSections.accordion("refresh");
}

function removePrintTypeHref() {
    let hrefToRemove = $(".property:contains('Manuscript or print') .values .resource a");
    hrefToRemove.css({ "text-decoration": "none", "color": "inherit" });
    hrefToRemove.removeAttr("href");
}