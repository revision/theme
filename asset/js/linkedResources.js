'use strict'
"use strict"

$(function () {

    // removeIncludedWorks();
    // removeDataSource();
    // findLinkedResources();
    // hideItemLinked();
    // removeEmptyProperties();
    // removeDropDown();

    function removeIncludedWorks() {
        $(".linked-resource-property:contains('Object')").parent().remove();
    }

    function findLinkedResources() {
        let LinkedResources = $("#linked-resources table tbody");
        LinkedResources.find('tr').each(function (i, element) {
            getLinkedData($(element).find('.resource-link').attr('href'));
            if (i === 0) {
                appendToMainSection($(this));
            }
        });
    }

    function removeDataSource() {
        $(".linked-resource-property:contains('hasPart')").parent().remove();
    }

    function hideItemLinked() {
        $("#item-linked").hide();
    }

    function removeDropDown() {
        $(".linked-header").remove();
        $("#ui-id-2").remove();
    }

    function removeEmptyProperties() {
        let PropertiesSection = $(".properties");

        PropertiesSection.find('.property').each(function (i, element) {
            if ($(this).children().text().trim().length == 0) {
                $(this).remove();
            }
        })
    }

    /* function appendToMainSection(linkedResource) {
        let PropertiesSection = $(".properties");

        PropertiesSection.append(
            "<div class=\"property\"><h4>" +
            linkedResource.find('.resource-class').text() +
            "</h4><div class=\"values\"></div></div>"
        );
    }

    async function getLinkedData(elementURL) {
        let id = elementURL.substring(pathname.lastIndexOf('/') + 1)
        let propertiesToWrite = [];

        // Each action has a lot of properties, many of which we are not interested in. These are the relevant ones.
        let relevantProperties = [
            "bdm2:hasType",
            "bdm2:carriedOutBy",
            "bdm2:movedTo",
            "rdfs:label"
        ];

        $.get(`/api/items/${id[0]}`, function (data) {
            for (const property in data) {
                if (relevantProperties.includes(property)) {
                    propertiesToWrite.push({ key: property, value: data[property] });
                }
            }
        }).done(function (data) {
            writeToHtml(propertiesToWrite, id);
        });
    }

    async function writeToHtml(linkedData, id) {
        let propertiesToWrite = linkedData;
        let accordionContainer = $(".property:contains('Object of') .values");

        accordionContainer.append('<div class=\"value\ item id_' + id + '">');
        let correctHTML = $(".value.id_" + id);

        correctHTML.append('<a class=\"link\"><span class=\"resource-name\">' + getAccordionHeader(propertiesToWrite) + '</span></a>');
        correctHTML.append('<span class=\"accordionContainer_' + id + '">');

        let actionContainer = $('.value.item.id_' + id + ' > .accordionContainer_' + id);

        // This should really be refactored
        for (let i = 0; i < propertiesToWrite.length; i++) {
            if (propertiesToWrite[i].value[0]['@value'] && propertiesToWrite[i].key != 'rdfs:label') {
                actionContainer.append(
                    "<div class=\"property\"><h4>" +
                    propertiesToWrite[i].value[0].property_label +
                    "</h4> <div class=\"actionValues\"> <div class=\"actionValue\" lang>" +
                    propertiesToWrite[i].value[0]['@value'] +
                    "</div></div>" +
                    "</div>");
            } else if (propertiesToWrite[i].key == 'bdm2:carriedOutBy' || propertiesToWrite[i].key == 'bdm2:movedTo' || propertiesToWrite[i].key == 'bdm2:hasType') {
                actionContainer.append(
                    "<div class=\"property\"><h4>" +
                    propertiesToWrite[i].value[0].property_label +
                    "</h4> <div class=\"accordionvalues\"> <div class=\"accordionvalue\" lang> " + createLink(propertiesToWrite[i].value[0]) +
                    propertiesToWrite[i].value[0].display_title +
                    "</a>" +
                    "</div></div>" +
                    "</div>");
            }
        }

        actionContainer.append("<div class=\"property\"><h4>Link to resource</h4>" + "<div class=\"accordionvalues\"> <div class=\"accordionvalue\" lang>" + "<a href=" + getPathName() + id + " target=_blank>" + 'Click to open in new window' + "</a>" + "</div></div></div>");
        accordionContainer.append('</span>');
        accordionContainer.append('</div>');
        createAccordion();
    }

    function getAccordionHeader(properties) {
        for (let i = 0; i < properties.length; i++) {
            if (properties[i].key === 'rdfs:label') {
                return properties[i].value[0]['@value']
            }
        };
    }

    function createAccordion() {
        $(".property:contains('Object of') .values .item").accordion({
            collapsible: true,
            active: false,
            animate: false
        });
    }

    function createLink(objectID) {
        let id = objectID['@id'].match(/([\d]+)/g);
        console.log('objectID: ', id)
        return `<a href=${getPathName() + id} target="_blank" style="color: #0000FF">`;
    }

    function getPathName() {
        let pathName = window.location.pathname;
        return pathName.replace(/\d/g, '');
    } */
});