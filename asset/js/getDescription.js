'use strict'
"use strict"

$(function () {

    function findDescriptions() {
        let items = $(".resource-list").find('li');

        items.each(function (index) {
            let text = $(this).find('.description').text();
            if (text.includes('/s/birgitta/item')) {
                getCorrectDescription($(this));
            }
        })
    }

    function getCorrectDescription(itemToChange) {
        let url = String($(itemToChange).find('.description').text());
        let id = (url.substring(url.lastIndexOf('/') + 1));
        $.get(`/api/items/${id}}`, function (data) {
            writeCorrectDescription(itemToChange, data['o:title'], url);
        });
    }

    function writeCorrectDescription(itemToChange, correctDescription, url) {
        $(itemToChange).find('.description').text(correctDescription);
    }

    findDescriptions()
});

