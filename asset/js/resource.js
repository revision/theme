'use strict'
"use strict"


// As per 26.06.20, this scripts only task is to replace 'Item' with the corresponding class for each resource. This needs to be done by custom JS because Omeka does not support display class name.

$(function () {

    let pathname = window.location.pathname
    let id = pathname.substring(pathname.lastIndexOf('/') + 1)

    let descriptionText = $(".subtitle")

    $.get(`/api/items/${id}}`, function (data) {
        let classToDisplay = mapToClass(data)
        descriptionText.html(classToDisplay)
    });

})


function mapToClass(rawClass) {
    if (!rawClass) {
        return null;
    };

    let dataClass = rawClass['@type'][1];
    let modelClass = dataClass.replace('bdm2:', '');

    // We only need to do something to the classes that are compounds and the action, which displays the action type as description
    switch (modelClass) {
        case 'BookObject':
            return 'Book Object'
        case 'WorkItem':
            return 'Work Item'
        case 'DataSource':
            return 'Data Source'
        case 'Action':
            hideActionLabel();
        // return rawClass['rdfs:label'][0]['@value']
        default:
            return modelClass
    }
}

function hideActionLabel() {
    $(".property").find("h4:contains('Action label')").parent().hide();
}
